import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './components/store.js'

// import MobMenu from './mobile-menu'
import PorgressBar from './progress-bar'

Vue.use(VueRouter)

import Home from './components/Home.vue'
import Docs from './components/Docs.vue'
import EventsPage from './components/EventsPage.vue'
import App from './components/App.vue'


const router = new VueRouter({
  mode: 'history',
  base: '/user',
  routes: [
    {path: '/', component: Home},
    {path: '/docs', component: Docs},
    {path: '/events', component: EventsPage}
  ]
})





document.addEventListener('DOMContentLoaded', () => {
  new Vue({
    router,
    template: '<App/>',
    components: {App},
  }).$mount('#app')
})


