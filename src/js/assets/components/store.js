export let allData = {
  'progress': {
    'stages': [
      {
        'title': {
          'label': 'Название этапа',
          'value': 'Начало'
        },
        'field_date_start': {
          'label': 'Дата начала',
          'value': '02.07.2018'
        },
        'field_date_end': {
          'label': 'Дата завершения',
          'value': '28.07.2018'
        },
        'field_body': {
          'label': 'Описание',
          'value': [
            {
              'value': '<p>Это начало начала</p>',
              'format': 'basic_html'
            }
          ]
        }
      },
      {
        'title': {
          'label': 'Название этапа',
          'value': 'Средний'
        },
        'field_date_start': {
          'label': 'Дата начала',
          'value': '18.07.2018'
        },
        'field_date_end': {
          'label': 'Дата завершения',
          'value': '26.07.2018'
        },
        'field_body': {
          'label': 'Описание',
          'value': []
        }
      },
      {
        'title': {
          'label': 'Название этапа',
          'value': ''
        },
        'field_date_start': {
          'label': 'Дата начала',
          'value': '01.01.1970'
        },
        'field_date_end': {
          'label': 'Дата завершения',
          'value': '01.01.1970'
        },
        'field_body': {
          'label': 'Описание',
          'value': []
        }
      },
      {
        'title': {
          'label': 'Название этапа',
          'value': ''
        },
        'field_date_start': {
          'label': 'Дата начала',
          'value': '01.01.1970'
        },
        'field_date_end': {
          'label': 'Дата завершения',
          'value': '01.01.1970'
        },
        'field_body': {
          'label': 'Описание',
          'value': []
        }
      },
      {
        'title': {
          'label': 'Название этапа',
          'value': ''
        },
        'field_date_start': {
          'label': 'Дата начала',
          'value': '01.01.1970'
        },
        'field_date_end': {
          'label': 'Дата завершения',
          'value': '01.01.1970'
        },
        'field_body': {
          'label': 'Описание',
          'value': []
        }
      }
    ],
    'current_stage': {
      'current_number_stages': '2/6',
      'fields': {
        'title': {
          'label': 'Название этапа',
          'value': 'Средний'
        },
        'field_date_start': {
          'label': 'Дата начала',
          'value': '18.07.2018'
        },
        'field_body': {
          'label': 'Описание',
          'value': []
        }
      }
    },
    'numbers_stage': [
      {
        'title': '01',
        'active': true
      },
      {
        'title': '02',
        'active': true
      },
      {
        'title': '03',
        'active': false
      },
      {
        'title': '04',
        'active': false
      },
      {
        'title': '05',
        'active': false
      }
    ]
  },
  'documents': [
    {
      'title': {
        'label': 'Название документа',
        'value': 'Дока'
      },
      'field_date': {
        'label': 'Дата',
        'value': '14.07.2018'
      },
      'field_link': {
        'label': 'Ссылка',
        'value': 'https://google.com'
      },
      'field_body': {
        'label': 'Подсказка',
        'value': [
          {
            'value': '<p>Здесь будет дока</p>',
            'format': 'basic_html'
          }
        ]
      }
    }
  ],
  'events': [
    {
      'field_date': {
        'label': 'Дата',
        'value': '14 июля'
      },
      'field_body': {
        'label': 'Описание',
        'value': [
          {
            'value': '<p>Мы сделали это</p>',
            'format': 'basic_html'
          }
        ]
      }
    }
  ],
  'user_info': {
    'name': 'Тестов Тест Тестович',
    'company': 'ООО Тест'
  }
}