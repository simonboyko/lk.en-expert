console.log('INIT mobile-menu.js');

document.addEventListener('DOMContentLoaded', () => {

  let hamburger = document.querySelector('.hamburger');
  let menu = document.querySelector('.page__menuBar');
  let page = document.querySelector('.page');
  let currentState = 'closed';

  hamburger.addEventListener('click', function (e) {
    var el = e.target;

    if (el.classList.contains('hamburger--opened')) {
      closeMenu();
      el.classList.remove('hamburger--opened');
    } else {
      openMenu();
      el.classList.add('hamburger--opened');
    }

  });



  function openMenu() {

    if (currentState === 'closed') {
      menu.classList.add('page__menuBar--open');
      currentState = 'opened';
      page.classList.add('page--noScroll');
    }

    console.log('f: openMenu');
  }

  function closeMenu() {

    if (currentState === 'opened') {
      menu.classList.remove('page__menuBar--open');
      currentState = 'closed';
      page.classList.remove('page--noScroll');
    }

    console.log('f: closeMenu');
  }


})



